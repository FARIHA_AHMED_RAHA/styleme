<html>
<head>
<title>ProjectSRS 327</title>

<link href="style.css" type="text/css" rel="stylesheet">
</head>

<body>
<h1>SOFTWARE REQUIREMENT SPECIFICATION FOR 'STYLE ME'</h1>

<div id="main">

 <h2>Table Of Contents:</h2>
 <hr>
 
 <nav>
 <ol class= "nav">
    <li> <a href="introduction.html">Introduction </a> </li>
	
	
    <li> <a href="overall description.html">Overall Description </a> </li>
	
    <li> <a href="external interface req.html">External Interface Requirements</a> </li>
	
    <li> <a href="system features.html">System Features</a> </li>
	
    <li> <a href="non-funtional req.html">Other Non-functional Requirements</a> </li>
	
    <li> <a href="other req.html">Other Requirements</a> </li>
	
	</ol>
	</nav>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<button onclick="window.print()">PRINT</button>
	</div>
	<footer>
	Created by --Najija Tasnim Khan--
	--Fariha Ahmed--
	--Asif Rahman--
	--19/10/2019--
	
	<br>
	page-1
	</footer>
	
</body>
</html>